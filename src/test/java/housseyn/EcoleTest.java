package housseyn;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class EcoleTest {
	

	    @Test
	    public void testAbsent() {
	        
	        Eleve eleve = new Eleve("Tioaule", "Ayoub", Age.ONZE,NiveauClasse.CM2);

	        eleve.setAbsent(true);
	        System.out.println("La valeur de 'absent' : " + eleve.getAbsent());

	        assertEquals(true, eleve.getAbsent());
	        
	        eleve.setAbsent(false);
	        System.out.println("La valeur de 'absent' : " + eleve.getAbsent());
	        
	        assertEquals(false, eleve.getAbsent());
	    }
	        	        

	        @Test
	        public void testGSEnsignat() {
	        		        		        	
	            Enseignant enseignant = new Enseignant("LAMIRAULT", "Xavier");
	            enseignant.setSalaire(2750.0);
	            enseignant.setNbAbsences(4);

	            assertEquals("LAMIRAULT", enseignant.getNom());
	            assertEquals("Xavier", enseignant.getPrenom());
	            assertEquals(2750.0, enseignant.getSalaire(),0.01);
	            assertEquals(4, enseignant.getNbAbsences());	            
	   	           
	        }
	        
	        @Test
	        public void testSePresenter() {
	        		Enseignant enseignant = new Enseignant("LAMIRAULT", "Xavier");
		           	enseignant.setSalaire(2750.0);
		            enseignant.setNbAbsences(4);

		            assertEquals("Bonjour je suis l'enseignant : Xavier LAMIRAULT. 	 Mon salaire : 2750.0 euros.	 Mes absences pour ce mois : 4",enseignant.sePresenter());
	           
	        }
	        
	        @Test
	        public void testGSPersonnel() {
	        	
	        	PersonnelAdministratif perAdmin = new PersonnelAdministratif("DUBOIS", "Pauline");
	        	perAdmin.setSalaire(2050);
	        	perAdmin.setNbAbsences(0);
	        	
	            assertEquals("DUBOIS", perAdmin.getNom());
	            assertEquals("Pauline", perAdmin.getPrenom());
	            assertEquals(2050, perAdmin.getSalaire(),0.01);
	            assertEquals(0, perAdmin.getNbAbsences());
	        }
	        
	        @Test
	        public void testAugmentSalaire() {
	        	        		        	
	        	Enseignant enseignant = new Enseignant("LAMIRAULT", "Xavier");
	        	PersonnelAdministratif perAdmin = new PersonnelAdministratif("DUBOIS", "Pauline");
	        	Ecole ecole = new Ecole();

	        	ecole.augmentPerso(enseignant, 2855.12);	        	
	   		 	ecole.augmentPerso(perAdmin,2422.53);
	   		 	
	   		 	assertTrue(enseignant.getSalaire()==2855.12);
	   		 	assertTrue(perAdmin.getSalaire()==2422.53);
	            	   	          
	        }
	        
	        public class EleveTest {

	        @Test
	         public void testNiveauClasse() {
	            Eleve eleve = new Eleve("EST","Julien",Age.DIX, NiveauClasse.CM1);
	            assertEquals(NiveauClasse.CE1, eleve.getNiveau());
	            }
	        
	        @Test
	         public void testAge() {
	            Eleve eleve = new Eleve("EST","Julien",Age.DIX, NiveauClasse.CM1);
	            assertEquals(Age.DIX, eleve.getAge());
	            }
	        
	        }
	        
	        @Test
	    	public void augmentationDeSalaire(){
	    		
	    		Enseignant enseignant = new Enseignant("LAMIRAULT", "Xavier");
	    		PersonnelAdministratif perAdmin = new PersonnelAdministratif("DUBOIS", "Pauline");
	    		Ecole ecole = new Ecole();
	    		enseignant.setSalaire(2750.0);
	    		
	    		 enseignant.setNbAbsences(7);
	    		 perAdmin.setNbAbsences(3);
	    		 
	    		 enseignant.demandAugment(ecole, enseignant, 3225.50);
	    		 perAdmin.demandAugment(ecole, perAdmin, 2599.99);

	    		 assertTrue(enseignant.getSalaire() == 2750.0);
	    		 assertTrue(perAdmin.getSalaire() == 2599.99);	    		 
	    	}
	        
	      
	    	@Test
	    	public void presentationStaffTest() {
	    		
	    		Enseignant enseignant1 = new Enseignant("LAMIRAULT", "Xavier");
	    		enseignant1.setSalaire(3750.0);
	    		enseignant1.setNbAbsences(4);
	    		Enseignant enseignant2 = new Enseignant("LEE", "Omar");
	    		enseignant2.setSalaire(2750.0);
	    		enseignant2.setNbAbsences(3);
	    		Enseignant enseignant3 = new Enseignant("ALPHA", "Xiao-Mi");
	    		enseignant3.setSalaire(2520.0);
	    		enseignant3.setNbAbsences(2);
	    		Enseignant enseignant4 = new Enseignant("MADELAINE", "fanta");
	    		enseignant4.setSalaire(4214.25);
	    		enseignant4.setNbAbsences(1);
	    		
	    		PersonnelAdministratif perAdmin1 = new PersonnelAdministratif("DUBOIS", "Pauline");
	    		perAdmin1.setSalaire(2520.0);
	    		perAdmin1.setNbAbsences(2);
	    		PersonnelAdministratif perAdmin2 = new PersonnelAdministratif("EST", "Julien");
	    		perAdmin2.setSalaire(2750.0);
	    		perAdmin2.setNbAbsences(3);
	    		PersonnelAdministratif perAdmin3 = new PersonnelAdministratif("ARNOLD", "Bachir");
	    		perAdmin3.setSalaire(1450.55);
	    		perAdmin3.setNbAbsences(1);

	    		List <EmployeEcole> listeEmployes = new ArrayList <EmployeEcole>();
	    		listeEmployes.add(enseignant1);
	    		listeEmployes.add(enseignant2);
	    		listeEmployes.add(enseignant3);
	    		listeEmployes.add(enseignant4);
	    		listeEmployes.add(perAdmin1);
	    		listeEmployes.add(perAdmin2);
	    		listeEmployes.add(perAdmin3);
	    		  
	    		  
	    		  for(EmployeEcole employe : listeEmployes){
	    			  employe.sePresenter();
	    		  }
	    		  
	    	}
	        
}
