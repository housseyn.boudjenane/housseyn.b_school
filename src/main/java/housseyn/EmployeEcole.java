package housseyn;

public interface EmployeEcole{
	
	public String sePresenter();
	
	public void setSalaire(double Salaire);
	public double getSalaire(); 
	
	public void setNbAbsences(int NbAbsences);	
	public int getNbAbsences();
	
	public void demandAugment(Ecole ecole, EmployeEcole empEco, double AugmentSalaire);
	
}