package housseyn;

	public class Enseignant extends Personne implements EmployeEcole {

		private double salaire;
	    private int NbAbsences;
		
	    public Enseignant(String nom, String prenom) {
			super(nom, prenom);
		}

		public double getSalaire() {
	        return salaire;
	    }

	    public void setSalaire(double salaire) {
	        this.salaire = salaire;
	    }

	    public int getNbAbsences() {
	        return NbAbsences;
	    }

	    public void setNbAbsences(int NbAbsences) {
	        this.NbAbsences = NbAbsences;
	    }



		public String sePresenter() {
			// TODO Auto-generated method stub
			String message = "Bonjour je suis l'enseignant : " + getPrenom() + " " + getNom() +". \t Mon salaire : " + salaire + " euros." + "\t Mes absences pour ce mois : " + NbAbsences;
			System.out.println(message);
			return message;
			
		}

		public void demandAugment(Ecole ecole, EmployeEcole enseignant, double AugmentSalaire) {
			System.out.println("Nouveau Salaire demandé : " + AugmentSalaire );
			ecole.augmentPerso(enseignant, AugmentSalaire);
			
		}		
	}
