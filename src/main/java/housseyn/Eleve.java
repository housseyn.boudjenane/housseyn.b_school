package housseyn;

public class Eleve extends Personne {
   
	// Variables d'instance
//	private String nom,prenom ;
    private Age age;
    private boolean absent;
    private NiveauClasse niveau;
    
	public Eleve(String nom, String prenom,Age age,NiveauClasse niveau) {
		super(nom, prenom);
		this.age=age;
		this.niveau=niveau;
		// TODO Auto-generated constructor stub
	}

//    public Eleve(String nom, String prenom, int age) {
//        this.nom = nom;
//        this.prenom = prenom;
//        this.age = age;
//    }


	public Age getAge() {
		return age;
	}

	public void setAge(Age age) {
		this.age = age;
	}


    public boolean getAbsent() {
        return absent;
    }

    public void setAbsent(boolean absent) {
        this.absent = absent;
    }


    public NiveauClasse getNiveau() {
        return niveau;
    }

    public void setNiveau(NiveauClasse niveau) {
    	System.out.println("Le niveau est : " + niveau);
        this.niveau = niveau;
    }

  
    public void apprendre() {
        System.out.println("L'élève apprend.");
    }
    
    
}
