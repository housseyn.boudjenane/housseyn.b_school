package housseyn;

public final class Ecole {
	
		public void augmentPerso(EmployeEcole empEco, double nSalaire) {
		if (empEco.getNbAbsences()<=4) {
			empEco.setSalaire(nSalaire);
			System.out.println("Augmentation Accéptée. Nouveau Salaire est : "+nSalaire);
		}else {
			System.out.println("Augmentation Refusée, nombre d'abcscences : " +empEco.getNbAbsences()+ " dépasse : 4 (seuil autorisé)");
			}		
		}
		
		public void assignerClasseEleve(Eleve eleve) {
			switch(eleve.getAge()) {
			case SIX : eleve.setNiveau(NiveauClasse.CP); break;
			case SEPT : eleve.setNiveau(NiveauClasse.CE1); break;
			case HUIT : eleve.setNiveau(NiveauClasse.CE2); break;
			case NEUF : eleve.setNiveau(NiveauClasse.CM1); break;
			case DIX : eleve.setNiveau(NiveauClasse.CM2); break;
			default:
				break;
			}
		}
}